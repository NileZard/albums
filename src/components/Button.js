import React from 'react'
import {Text, TouchableOpacity} from 'react-native'
 

const Button= ({onPress, children}) => {
    return (
        <TouchableOpacity style={styles.buttonStyle} onPress={onPress}>
        <Text style={styles.textStyles}> {children} </Text>
        </TouchableOpacity>
 
    );
};

const styles = {

    buttonStyle:{
        borderWidth:1,
        borderRadius:5,
        borderColor:'#ddd',
        borderBottomWidth:0,
        shadowColor:'#000',
        backgroundColor:'#2196F3',
        shadowOffset:{width: 0, height:2},
        shadowOpacity: 0.1,
        shadowRadius: 2,
        elevation:1,
        marginTop :4,
        marginRight:4,
        marginLeft:4,
        marginBottom:8,
        flex:1,
        alignItems:"center",
        justifyContent:'center',
        height:36,
        width: null
    
    },

    textStyles:{
        fontSize:16,
        color:"#fff",
        justifyContent:'center',
        alignItems:"center",
    }


}
export default Button;