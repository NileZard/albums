import  React from 'react'
import {Text, View,Platform } from 'react-native'

const Header = (props) =>{
    const {textStyle, ViewStyle} = styles;
    return (
    
    <View style = {ViewStyle}>
         <Text style ={textStyle }>{props.headerText} </Text>

    </View>
         );
  
};

const instructions = Platform.select({
    ios:'center',
    android: 'flex-start',
  });

const styles = {

    ViewStyle:{

        height:64 ,
        backgroundColor:'#2196F3',
        justifyContent:'center',
        alignItems:instructions,
        paddingLeft:16,
        shadowColor:'#000',
        shadowOpacity:0.2,
        shadowOffset:{width:0, height:2}

    },
    textStyle:{

        fontSize: 20,
        color: '#ffffff'
        
    }

};

export default Header;