import React from 'react'
import {Text,View,Image,Linking} from 'react-native'
import Card from './Card'
import CardSection from './CardSection'
import Button from './Button'

const AlbumDetail =({album})=>{

    const {title, artist, thumbnail_image,image, url} = album;
    const {headerContentStyle, thumnailStyle,thumnailContainerStyle, imageStyle,headerTextStyle} = styles;
    return(

    <Card>
        <CardSection>
            <View style = {thumnailContainerStyle}>
                <Image
                    style={thumnailStyle}
                    source={{ uri: thumbnail_image}}
                />
            </View>
            <View style = {headerContentStyle}>
                <Text style={headerTextStyle}>{title}</Text>
                <Text >{artist}</Text>
            </View>
            
        </CardSection>
        <Image style = {imageStyle}
                source={{uri:image}}
            />
    
            <Button  onPress={() => Linking.openURL(url)}>
                Buy Now
            </Button>
     
    </Card>
    
    );
};

const styles = {

    headerContentStyle:{
        flexDirection:'column',
        justifyContent:'space-around',
        margin:4
    },
    thumnailStyle:{
        width: 72, 
        height: 72
    },
    thumnailContainerStyle:{
       justifyContent:'center',
       alignItems:'center',
       marginLeft: 8,
       marginRight: 8,


    },
    imageStyle:{
        margin :8 ,
        height: 300,
        flex:1,
        width: null
    },

    headerTextStyle:{
    fontSize:18

    }
};


  

export default AlbumDetail